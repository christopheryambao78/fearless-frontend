import React from 'react';
import Nav from './Nav';
import PresentationForm from './PresentationForm';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeSignupForm from './AttendeeSignupForm';
import { BrowserRouter } from "react-router-dom"
import { Routes } from 'react-router-dom';
import { Route } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <div className="container">
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm/>}/>
          </Route>
          {/* <Route path="attendees">
            <Route path="" element={<AttendeesList/>}/>
          </Route> */}
          <Route path="attendees">
            <Route path="new" element={<AttendeeSignupForm/>}/>
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
