import React from "react";

class PresentationhtmlForm extends React.Component {
    constructor(props){
        super(props)

    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form id="create-presentation-htmlForm">
                  <div className="htmlForm-floating mb-3">
                    <input placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="htmlForm-control"></input>
                    <label htmlFor="presenter_name">Presenter Name</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="presenter_email" className="htmlForm-label">Presenter Email</label>
                    <input type="presenter_email" className="htmlForm-control" name="presenter_email" id="presenter_email" placeholder="name@example.com"></input>
                  </div>
                  <div className="htmlForm-floating mb-3">
                    <input placeholder="Company name" type="text" name="company_name" id="company_name" className="htmlForm-control"></input>
                    <label htmlFor="company_name">Company Name</label>
                  </div>
                  <div className="htmlForm-floating mb-3">
                    <input placeholder="Title" required type="text" name="title" id="title" className="htmlForm-control"></input>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="synopsis" className="htmlForm-label">Synopsis</label>
                    <textarea className="htmlForm-control" required type="text" name="synopsis" id="synopsis" rows="3"></textarea>
                  </div>
                  <div className="mb-3">
                    <select required id="conferences" name="conferences" className="htmlForm-select">
                      <option selected value="">Choose a conference</option>
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}
export default PresentationhtmlForm;